import 'package:get/get.dart';
import 'package:ourvillage_wallet/models/balance_model.dart';

import 'package:ourvillage_wallet/models/login_response.dart';
import 'package:ourvillage_wallet/models/reset_pin_response_model.dart';
import 'package:ourvillage_wallet/models/transaction_model.dart';
import 'package:ourvillage_wallet/models/transfer_response_model.dart';
import 'package:ourvillage_wallet/models/user_model.dart';
import 'package:ourvillage_wallet/models/village_model.dart';
import 'package:ourvillage_wallet/screens/login_screen.dart';
import 'package:ourvillage_wallet/services/remote_services.dart';

class AppController extends GetxController {
  late User user;
  List<Village> villages = [
    Village(name: "Bameka", tokenName: "MUNKAP", tokenSymbole: "MUN"),
    Village(name: "Batoufam", tokenName: "MBIP TSEFAP", tokenSymbole: "MBIP"),
    Village(name: "Fondjomekwet", tokenName: "MBAM", tokenSymbole: "MBA"),
  ];

  late Village activeVillage;
  RemoteServices remoteService = RemoteServices();

  var transactions = <TransactionModel>[].obs;
  var balances = <BalanceModel>[].obs;

  RxInt balanceToShow = 0.obs;

  BalanceModel? activeBalance;

  Future<LoginResponse> login(phone, pin) async {
    var result = await remoteService.login(phone, pin);
    if(result.status == "success" && result.user != null){
      user = result.user as User;
      var searchVillage = villages.firstWhere((village) => user.village == village.name);
      if(searchVillage is Village) {
        activeVillage = searchVillage;
      }
    }
    return result;
  }

  Future<void> loadTransactions() async {
    transactions.value = await remoteService.getTransactions(user.blockchainAddress);
  }

  void updateBalanceToShow() {
    print(activeBalance?.balance);
    balanceToShow.value = activeBalance?.balance ?? 0;
  }

  Future<void> loadBalances() async {
    balances = (await remoteService.getBalances(user.phoneNumber)).obs;
    activeBalance = balances.firstWhere((ba) => ba.symbol.toLowerCase() == activeVillage.tokenSymbole.toLowerCase());
    updateBalanceToShow();
  }

  void changeActiveVillage(villageName) {
    var searchVillage = villages.firstWhere((village) => village.name.toLowerCase() == villageName);
    activeVillage = searchVillage;
    activeBalance = balances.firstWhere((ba) => ba.symbol.toLowerCase() == activeVillage.tokenSymbole.toLowerCase());
    updateBalanceToShow();
  }

  void clearAllAndLogout() {
    Get.offAll(const LoginScreen());
    transactions.value = [];
    remoteService = RemoteServices();
  }

  Future<ResetPinResponseModel> setPinCode(String oldPin, String newPin) async {
    return await remoteService.setPinCode(user.phoneNumber, oldPin, newPin);
  }

  Future<TranferResponseModel> transfer(String to, String amount, String pin, String reason) async {
    return await remoteService.transfer(to, user.phoneNumber, amount, pin, activeVillage.tokenSymbole, reason);
  }
}