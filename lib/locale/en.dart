import 'translation_keys.dart' as translation;

class En {
  Map<String, String> get messages => {
    translation.loginTitle: 'Login',
    translation.loginMessage: "Enter your login and password to access your portfolio.",
    translation.phoneNumberHelpText: "Phone number",
    translation.pinCodeHelpText: "PIN Code",
    translation.loginCreateAccount: "Create an account",
    translation.labelLanguage: "Language",
    translation.loginButton: "Login",

    translation.appTitleHome: 'Home',
    translation.appTitleTransactions: 'Transactions',
    translation.appTitleSettings: 'Settings',

    translation.descVillage: 'Choose the groupement',
    translation.descLanguage: 'Choose the language',
    translation.descPinCode: 'Change pin code',
    translation.labelVillage: 'Groupement',

    translation.labelCurrentPin: 'Current PIN code',
    translation.labelNewPin: 'New PIN code',
    translation.labelConfirmPin: 'Confirm PIN code',
    translation.buttonSave: 'Save',

    translation.buttonSent: 'Send',
    translation.buttonReceive: 'Receive',
    translation.buttonCummunityFund: 'Community fund',
    translation.descAccount: 'Your account balance',
    translation.descAccountActions: "What do you want to do today?",

    translation.descCreateAccount: "To create an account please type the code above at one of the operators (MTN or Orange) and then configure your password and return to the application.",
    translation.descCommunityFundBalance: "The balance in the community fund is:",
    translation.descReceiveVouchers: "To receive vouchers tell your sender to send the voucher to the number below, after sending you both will be notified via SMS of the status of the transaction.",
  
    translation.descSend: "Fill out the form to send a coupon to another user.",
    translation.descSendAmount: "Amount",
    translation.descSendService1: 'Agriculture product',
    translation.descSendService2: 'Vegetable product',
    translation.descSendService3: "Product of breeding", 
    translation.descSendService4: 'Artisanal product',
    translation.descSendService5: 'Service',

    translation.labelSentTransaction: 'Sent to @number',
    translation.labelRecipientTransaction: 'Received from @number',

    translation.labelLogout: 'Log out?',
  };
}