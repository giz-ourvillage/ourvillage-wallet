import "translation_keys.dart" as translation;

class Fr {
  Map<String, String> get messages => {
    translation.loginTitle: "Se connecter",
    translation.loginMessage: "Entrez vos identifiants afin d'acceder a votre portefeuille.",
    translation.phoneNumberHelpText: "Numero de telephone",
    translation.pinCodeHelpText: "Code PIN",
    translation.loginCreateAccount: "Creer un compte",
    translation.labelLanguage: "Langue",
    translation.loginButton: "Se connecter",

    translation.appTitleHome: 'Accueil',
    translation.appTitleTransactions: 'Transactions',
    translation.appTitleSettings: 'Parametres',

    translation.descVillage: 'Choissisez le village',
    translation.descLanguage: 'Choissisez la langue',
    translation.descPinCode: 'Changez de code pin',
    translation.labelVillage: 'Village',

    translation.labelCurrentPin: 'Code PIN actuel',
    translation.labelNewPin: 'Nouveau code PIN',
    translation.labelConfirmPin: 'Confirmer le code PIN',
    translation.buttonSave: 'Sauvegarder',

    translation.buttonSent: 'Envoyer',
    translation.buttonReceive: 'Recevoir',
    translation.buttonCummunityFund: 'Caisse communautaire',
    translation.descAccount: 'Le solde de votre compte',
    translation.descAccountActions: "Que voulez-vous faire aujourd'hui?",

    translation.descCreateAccount: "Pour créer un compte veuillez taper le code ci-dessus chez l'un des opérateur (MTN ou Orange) et ensuite configurer votre mot de passe puis revenez dans l'application.",
    translation.descCommunityFundBalance: "Le solde de la caisse communautaire est de:",
    translation.descReceiveVouchers: "Pour recevoir des bons dite a votre expéditeur d'envoyer le bon au numéro ci-dessous, après l'envoi vous tous deux allez être notifié via SMS du statut de la transaction.",

    translation.descSend: "Remplissez le formulaire afin d'envoyer un bon a un autre utilisateur.",
    translation.descSendAmount: "Montant",
    translation.descSendService1: 'Produit agricole',
    translation.descSendService2: 'Produit maraiche',
    translation.descSendService3: "Produit d'elevage", 
    translation.descSendService4: 'Produit artisanal',
    translation.descSendService5: 'Service',

    translation.labelSentTransaction: 'Envoyer a @number',
    translation.labelRecipientTransaction: 'Recu de @number',

    translation.labelLogout: 'Déconnectez-vous ?',

  };
}