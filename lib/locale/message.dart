import 'package:get/get.dart';
import 'package:ourvillage_wallet/locale/en.dart';
import 'package:ourvillage_wallet/locale/fr.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': En().messages,
        'fr_FR': Fr().messages,
      };
}