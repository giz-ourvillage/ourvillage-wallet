// Login
const loginTitle = 'loginTitle';
const loginMessage = 'loginMessage';
const phoneNumberHelpText = 'phoneNumberHelpText';
const pinCodeHelpText = 'pinCodeHelpText';
const loginCreateAccount = 'loginCreateAccount';

// Label
const labelLanguage = 'labelLanguage';
const labelVillage = 'labelVillage';
const labelCurrentPin = 'labelCurrentPin';
const labelNewPin = 'labelNewPin';
const labelConfirmPin = 'labelConfirmPin';
const labelSentTransaction= 'labelSentTransaction';
const labelRecipientTransaction= 'labelRecipientTransaction';
const labelLogout= 'labelLogout';

// Button
const loginButton = 'LoginButton';
const buttonSave = 'buttonSave';
const buttonSent = 'buttonSent';
const buttonReceive = 'buttonReceive';
const buttonCummunityFund = 'buttonCummunityFund';

// AppTitle
const appTitleHome = 'appTitleHome';
const appTitleTransactions = 'appTitleTransactions';
const appTitleSettings = 'appTitleSettings';

// Desc
const descVillage = 'descVillage';
const descLanguage = 'descLanguage';
const descPinCode = 'descPinCode';
const descAccount = 'descAccount';
const descAccountActions = 'descAccountActions';
const descCreateAccount = 'descCreateAccount';
const descCommunityFundBalance = 'descCommunityFundBalance';
const descReceiveVouchers = 'descReceiveVouchers';

const descSend = 'descSend';
const descSendAmount = 'descSendAmount';
const descSendService1= 'descSendService1';
const descSendService2= 'descSendService2';
const descSendService3= 'descSendService3';
const descSendService4= 'descSendService4';
const descSendService5= 'descSendService5';
