import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:ourvillage_wallet/locale/message.dart';
import 'package:ourvillage_wallet/screens/login_screen.dart';
import 'package:get/get.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future main() async {
  await dotenv.load(fileName: ".env");
  return runApp(GetMaterialApp(
    home: const LoaderOverlay(
      child: LoginScreen(),
    ),
    translations: Messages(),
    locale: Get.deviceLocale,
    fallbackLocale: const Locale('fr', 'FR'),
    debugShowCheckedModeBanner: false,
  ));
}

