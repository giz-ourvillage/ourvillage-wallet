// To parse this JSON data, do
//
//     final balanceModel = balanceModelFromJson(jsonString);

import 'dart:convert';

List<BalanceModel> balanceModelFromJson(String str) => List<BalanceModel>.from(json.decode(str).map((x) => BalanceModel.fromJson(x)));

String balanceModelToJson(List<BalanceModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BalanceModel {
    BalanceModel({
        required this.balance,
        required this.symbol,
    });

    int balance;
    String symbol;

    factory BalanceModel.fromJson(Map<String, dynamic> json) => BalanceModel(
        balance: json["balance"],
        symbol: json["symbol"],
    );

    Map<String, dynamic> toJson() => {
        "balance": balance,
        "symbol": symbol,
    };
}
