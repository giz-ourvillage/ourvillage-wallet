// To parse this JSON data, do
//
//     final CommunityFundModel = communityFundFromJson(jsonString);

import 'dart:convert';

CommunityFundModel communityFundFromJson(String str) => CommunityFundModel.fromJson(json.decode(str));

String communityFundToJson(CommunityFundModel data) => json.encode(data.toJson());

class CommunityFundModel {
    CommunityFundModel({
        required this.mba,
        required this.mbip,
        required this.mun,
    });

    int mba;
    int mbip;
    int mun;

    factory CommunityFundModel.fromJson(Map<String, dynamic> json) => CommunityFundModel(
        mba: json["MBA"],
        mbip: json["MBIP"],
        mun: json["MUN"],
    );

    Map<String, dynamic> toJson() => {
        "MBA": mba,
        "MBIP": mbip,
        "MUN": mun,
    };
}
