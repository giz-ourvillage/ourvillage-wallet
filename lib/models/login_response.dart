// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

import 'package:ourvillage_wallet/models/user_model.dart';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
    LoginResponse({
        required this.status,
        this.accessToken,
        this.user,
        this.msg,
    });

    String? accessToken;
    String status;
    User? user;
    String? msg;

    factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        accessToken: json["access_token"],
        status: json["status"],
        user: json["user"] != null ? User.fromJson(json["user"]): null,
        msg: json["msg"],
    );

    Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "status": status,
        "user": user?.toJson(),
        "msg": msg,
    };
}

