// To parse this JSON data, do
//
//     final resetPinResponseModel = resetPinResponseModelFromJson(jsonString);

import 'dart:convert';

ResetPinResponseModel resetPinResponseModelFromJson(String str) => ResetPinResponseModel.fromJson(json.decode(str));

String resetPinResponseModelToJson(ResetPinResponseModel data) => json.encode(data.toJson());

class ResetPinResponseModel {
    ResetPinResponseModel({
        required this.status,
        required this.message,
    });

    String status;
    String message;

    factory ResetPinResponseModel.fromJson(Map<String, dynamic> json) => ResetPinResponseModel(
        status: json["status"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
    };
}
