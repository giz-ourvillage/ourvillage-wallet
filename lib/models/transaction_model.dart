// To parse this JSON data, do
//
//     final transactionModel = transactionModelFromJson(jsonString);

import 'dart:convert';

List<TransactionModel> transactionModelFromJson(String str) => List<TransactionModel>.from(json.decode(str).map((x) => TransactionModel.fromJson(x)));

String transactionModelToJson(List<TransactionModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TransactionModel {
    TransactionModel({
        required this.actionTag,
        required this.altBlockchainAddress,
        required this.altMetadataId,
        required this.blockchainAddress,
        required this.directionTag,
        required this.metadataId,
        required this.phoneNumber,
        required this.preferredLanguage,
        required this.role,
        required this.timestamp,
        required this.tokenDecimals,
        required this.tokenSymbol,
        required this.tokenValue,
    });

    String actionTag;
    String altBlockchainAddress;
    String altMetadataId;
    String blockchainAddress;
    String directionTag;
    String metadataId;
    String phoneNumber;
    String preferredLanguage;
    String role;
    int timestamp;
    int tokenDecimals;
    String tokenSymbol;
    int tokenValue;

    factory TransactionModel.fromJson(Map<String, dynamic> json) => TransactionModel(
        actionTag: json["action_tag"],
        altBlockchainAddress: json["alt_blockchain_address"],
        altMetadataId: json["alt_metadata_id"],
        blockchainAddress: json["blockchain_address"],
        directionTag: json["direction_tag"],
        metadataId: json["metadata_id"],
        phoneNumber: json["phone_number"],
        preferredLanguage: json["preferred_language"],
        role: json["role"],
        timestamp: json["timestamp"],
        tokenDecimals: json["token_decimals"],
        tokenSymbol: json["token_symbol"],
        tokenValue: json["token_value"],
    );

    Map<String, dynamic> toJson() => {
        "action_tag": actionTag,
        "alt_blockchain_address": altBlockchainAddress,
        "alt_metadata_id": altMetadataId,
        "blockchain_address": blockchainAddress,
        "direction_tag": directionTag,
        "metadata_id": metadataId,
        "phone_number": phoneNumber,
        "preferred_language": preferredLanguage,
        "role": role,
        "timestamp": timestamp,
        "token_decimals": tokenDecimals,
        "token_symbol": tokenSymbol,
        "token_value": tokenValue,
    };
}
