// To parse this JSON data, do
//
//     final tranferResponseModel = tranferResponseModelFromJson(jsonString);

import 'dart:convert';

TranferResponseModel tranferResponseModelFromJson(String str) => TranferResponseModel.fromJson(json.decode(str));

String tranferResponseModelToJson(TranferResponseModel data) => json.encode(data.toJson());

class TranferResponseModel {
    TranferResponseModel({
        required this.status,
        required this.msg,
    });

    String status;
    String msg;

    factory TranferResponseModel.fromJson(Map<String, dynamic> json) => TranferResponseModel(
        status: json["status"],
        msg: json["msg"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "msg": msg,
    };
}
