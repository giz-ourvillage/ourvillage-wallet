// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        required this.created,
        required this.updated,
        required this.id,
        required this.blockchainAddress,
        required this.phoneNumber,
        this.fullName,
        required this.username,
        required this.failedPinAttempts,
        required this.status,
        required this.preferredLanguage,
        this.guardians,
        required this.guardianQuora,
        required this.village,
    });

    String created;
    String updated;
    int id;
    String blockchainAddress;
    String phoneNumber;
    dynamic fullName;
    String username;
    int failedPinAttempts;
    int status;
    String preferredLanguage;
    dynamic guardians;
    int guardianQuora;
    String village;

    factory User.fromJson(Map<String, dynamic> json) => User(
        created: json["created"],
        updated: json["updated"],
        id: json["id"],
        blockchainAddress: json["blockchain_address"],
        phoneNumber: json["phone_number"],
        fullName: json["full_name"],
        username: json["username"],
        failedPinAttempts: json["failed_pin_attempts"],
        status: json["status"],
        preferredLanguage: json["preferred_language"],
        guardians: json["guardians"],
        guardianQuora: json["guardian_quora"],
        village: json["village"],
    );

    Map<String, dynamic> toJson() => {
        "created": created,
        "updated": updated,
        "id": id,
        "blockchain_address": blockchainAddress,
        "phone_number": phoneNumber,
        "full_name": fullName,
        "username": username,
        "failed_pin_attempts": failedPinAttempts,
        "status": status,
        "preferred_language": preferredLanguage,
        "guardians": guardians,
        "guardian_quora": guardianQuora,
        "village": village,
    };
}
