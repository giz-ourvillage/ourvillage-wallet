// To parse this JSON data, do
//
//     final village = villageFromJson(jsonString);

import 'dart:convert';

Village villageFromJson(String str) => Village.fromJson(json.decode(str));

String villageToJson(Village data) => json.encode(data.toJson());

class Village {
    Village({
        required this.name,
        required this.tokenName,
        required this.tokenSymbole,
    });

    String name;
    String tokenName;
    String tokenSymbole;

    factory Village.fromJson(Map<String, dynamic> json) => Village(
        name: json["name"],
        tokenName: json["token_name"],
        tokenSymbole: json["token_symbole"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "token_name": tokenName,
        "token_symbole": tokenSymbole,
    };
}
