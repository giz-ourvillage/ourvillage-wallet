import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/widgets/main_account.dart';
import 'package:ourvillage_wallet/widgets/transactions.dart';
import 'package:ourvillage_wallet/widgets/setting.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  final AppController appController = Get.put(AppController());

  List<String> appBarTitle = [translation.appTitleHome, translation.appTitleTransactions, translation.appTitleSettings];
  List<Widget> menu = [const MainAccount(), const Transactions(), const Setting()];

  String getAppBarTitle(int key) {
    return appBarTitle[key];
  }

  Widget getSubMenu() {
    return menu[_currentIndex];
  }

  @override
  initState() {
    super.initState();
    appController.loadTransactions();
    appController.loadBalances();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: AppBar(
      title: Text(getAppBarTitle(_currentIndex).tr),
      centerTitle: true,
      backgroundColor: Colors.red,
      elevation: 0,
    ),
    body: Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: getSubMenu(),
      ),
    ),
    bottomNavigationBar: BottomNavigationBar(
      currentIndex: _currentIndex,
      items: [
        BottomNavigationBarItem(
          icon: const Icon(Icons.home),
          backgroundColor: Colors.red,
          label: translation.appTitleHome.tr
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.history),
          backgroundColor: Colors.red,
          label: translation.appTitleTransactions.tr
        ),
        BottomNavigationBarItem(
          icon: const Icon(Icons.settings),
          backgroundColor: Colors.red,
          label: translation.appTitleSettings.tr
        ),
      ],
      selectedItemColor: Colors.red,
      iconSize: 22,
      elevation: 0,
      onTap: (value) {
        setState(() => _currentIndex = value);
      },
    ),
  );
  }
}