import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:get/get.dart";
import "package:loader_overlay/loader_overlay.dart";
import "package:ourvillage_wallet/controllers/app_controller.dart";
import "package:ourvillage_wallet/screens/home_screen.dart";
import "package:ourvillage_wallet/utils/bottom_sheet.dart";
import "package:ourvillage_wallet/validators.dart";
import "package:ourvillage_wallet/widgets/create_account.dart";
import "package:ourvillage_wallet/widgets/language_selection.dart";
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final AppController appController = Get.put(AppController());
  TextEditingController phoneController = TextEditingController();  
  TextEditingController pinController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed:  (){
                        openBottomSheet(
                          content: const LanguageSelection(),
                          height: 200,
                        );
                      },
                      child: Text(
                        translation.labelLanguage.tr,
                        style: const TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 35,),
                Text(
                  translation.loginTitle.tr,
                  style: const TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                const SizedBox(height: 18.0),
                Text(
                  translation.loginMessage.tr,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
                const SizedBox(height: 24.0),
                TextFormField(
                  decoration: InputDecoration(
                    filled: true,
                    labelText: translation.phoneNumberHelpText.tr,
                  ),
                  controller: phoneController,
                  validator: (value){
                    return isValidPhoneNumber(value ?? "");
                  },
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 14.0),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: translation.pinCodeHelpText.tr,
                  ),
                  controller: pinController,
                  validator: (value){
                    return isValidPin(value ?? "");
                  },
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 14.0),
                FilledButton(
                  onPressed: () async {
                    if(_formKey.currentState!.validate()) {
                      context.loaderOverlay.show();
                      var result = await appController.login(phoneController.text, pinController.text);
                      context.loaderOverlay.hide();
                      if(result.status == "success"){
                        Get.off(const HomeScreen());
                      } else {
                        Get.snackbar('Error', "${result.msg}", snackPosition: SnackPosition.BOTTOM);
                      }
                    }
                  },
                  style: FilledButton.styleFrom(
                    backgroundColor: Colors.red,
                    minimumSize: const Size.fromHeight(50),
                  ),
                  child: Text(
                    translation.loginButton.tr,
                  ),
                ),
                const SizedBox(height: 14.0),
                TextButton(
                  onPressed: (){
                    openBottomSheet(
                      content: const CreateAccount(),
                      height: 230,
                    );
                  },
                  child: Text(
                    translation.loginCreateAccount.tr,
                    style: const TextStyle(
                      color: Colors.red,
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}