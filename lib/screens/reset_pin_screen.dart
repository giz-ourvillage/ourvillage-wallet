import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;
import 'package:ourvillage_wallet/validators.dart';

class ResetPinScreen extends StatefulWidget {
  const ResetPinScreen({super.key});

  @override
  State<ResetPinScreen> createState() => _ResetPinScreenState();


}

class _ResetPinScreenState extends State<ResetPinScreen> {
  final _formKey = GlobalKey<FormState>();
  final AppController appController = Get.put(AppController());
  TextEditingController currentPin = TextEditingController();  
  TextEditingController newPin = TextEditingController();
  TextEditingController confirmNewPin = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return LoaderOverlay(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Code PIN"),
          backgroundColor: Colors.red,
           centerTitle: true,
           elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: translation.labelCurrentPin.tr,
                  ),
                  validator: (value){
                    return isValidPin(value ?? "");
                  },
                  controller: currentPin,
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 14.0),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: translation.labelNewPin.tr,
                  ),
                  validator: (value){
                    return isValidPin(value ?? "");
                  },
                  controller: newPin,
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 14.0),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: translation.labelConfirmPin.tr,
                  ),
                  validator: (value){
                    return confirmPin(newPin.text, value ?? "");
                  },
                  controller: confirmNewPin,
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 14.0),
                FilledButton(
                  onPressed: () async {
                    if(_formKey.currentState!.validate()) {
                      context.loaderOverlay.show();
                      var result = await appController.setPinCode(currentPin.text, newPin.text);
                      context.loaderOverlay.hide();
                      if(result.status == "success"){
                        appController.clearAllAndLogout();
                      } else {
                        Get.snackbar('Error', result.message, snackPosition: SnackPosition.BOTTOM);
                      }
                    }
                  },
                  style: FilledButton.styleFrom(
                    backgroundColor: Colors.red,
                    minimumSize: Size.fromHeight(50),
                  ),
                  child: Text(
                    translation.buttonSave.tr,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}