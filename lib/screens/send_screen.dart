import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;
import 'package:ourvillage_wallet/screens/home_screen.dart';
import 'package:ourvillage_wallet/validators.dart';

class SenDScreen extends StatefulWidget {
  const SenDScreen({super.key});

  @override
  State<SenDScreen> createState() => _SenDScreenState();
}

class _SenDScreenState extends State<SenDScreen> {
  final _formKey = GlobalKey<FormState>();
  final AppController appController = Get.put(AppController());
  TextEditingController phoneNumberController = TextEditingController();  
  TextEditingController amountController = TextEditingController();  
  TextEditingController pinController = TextEditingController();  

  List<String> types = [
    translation.descSendService1.tr,
    translation.descSendService2.tr,
    translation.descSendService3.tr,
    translation.descSendService4.tr,
    translation.descSendService5.tr,
  ];

  String selectedType = translation.descSendService1.tr;

  @override
  Widget build(BuildContext context) {
    return LoaderOverlay(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            appController.activeVillage.tokenSymbole
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.red,
        ),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(15, 5, 15, 0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 18.0),
                  Text(
                    translation.descSend.tr,
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 24.0),
                  TextFormField(
                    decoration: InputDecoration(
                      filled: true,
                      labelText: translation.phoneNumberHelpText.tr,
                    ),
                    validator: (value){
                      var result =  isValidPhoneNumber(value ?? "");
                      if(result != null) return result;
                      if(value == appController.user.phoneNumber.substring(3)){
                        return "Change to another number";
                      }
                      return null;
                    },
                    controller: phoneNumberController,
                    keyboardType: TextInputType.number,
                  ),
                  const SizedBox(height: 14.0),
                  TextFormField(
                    decoration: InputDecoration(
                      filled: true,
                      labelText: translation.descSendAmount.tr,
                    ),
                    controller: amountController,
                    validator: (value){
                      return validateAmountToSend(value ?? "", appController.activeBalance?.balance ?? 0);
                    },
                    keyboardType: TextInputType.number,
                  ),
                  const SizedBox(height: 14.0),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      filled: true,
                      labelText: translation.pinCodeHelpText.tr,
                    ),
                    controller: pinController,
                    validator: (value){
                      return isValidPin(value ?? "");
                    },
                    keyboardType: TextInputType.number,
                  ),
                  const SizedBox(height: 14.0),
                  DropdownButton(
                    items: types.map(
                      (String item) => DropdownMenuItem(
                        value: item.tr,
                        child: Text(item.tr)
                      )
                    ).toList(),
                    onChanged: (String? value){
                      setState(() {
                        selectedType = value ?? translation.descSendService1.tr;
                      });
                    },
                    isExpanded: true,
                    value: selectedType.tr,
                  ),
                  const SizedBox(height: 14.0),
                  FilledButton(
                    onPressed: () async {
                      if(_formKey.currentState!.validate()) {
                        context.loaderOverlay.show();
                        var result = await appController.transfer(phoneNumberController.text, amountController.text, pinController.text, selectedType.tr);
                        context.loaderOverlay.hide();
                        if(result.status == 'success'){
                          Get.snackbar('Success', result.msg, snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.green, colorText: Colors.white, duration: const Duration(seconds: 6));
                          Get.off(HomeScreen());
                        }else{
                          Get.snackbar('Error', result.msg, snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.red, colorText: Colors.white);
                        }
                      }
                    },
                    style: FilledButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size.fromHeight(50),
                    ),
                    child: Text(
                      translation.buttonSent.tr,
                    ),
                  ),
                ],
              ),
            ),
          )
      ),
    );
  }
}