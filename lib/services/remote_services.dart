import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:ourvillage_wallet/models/balance_model.dart';
import 'package:ourvillage_wallet/models/community_fund_model.dart';
import 'package:ourvillage_wallet/models/login_response.dart';
import 'package:ourvillage_wallet/models/reset_pin_response_model.dart';
import 'package:ourvillage_wallet/models/transaction_model.dart';
import 'package:ourvillage_wallet/models/transfer_response_model.dart';

class RemoteServices {
  var baseUrl = dotenv.env['API_URL'];
  var token = "";
  var client = http.Client();

  Future<LoginResponse> login(String phoneNumber, String pinCode) async {
    var response = await client.post(
      Uri.parse("$baseUrl/auth/login"),
      headers:{
        "Content-Type": "application/json"
      },
      body: jsonEncode({ 'phone': phoneNumber, 'pin': pinCode })
    );
    var loginRes =  loginResponseFromJson(response.body);
    if(loginRes.status == 'success'){
      token = loginRes.accessToken != null ? "${loginRes.accessToken}" : "";
    }
    return loginRes;
  }

  Future<CommunityFundModel> getCommunityFund() async {
    var response = await client.get(
      Uri.parse("$baseUrl/user/community_fund"),
      headers:{
        "Authorization": "Bearer $token"
      },
    );
    return communityFundFromJson(response.body);
  }

  Future<List<BalanceModel>> getBalances(String phoneNumber) async {
    var response = await client.post(
      Uri.parse("$baseUrl/auth/vouchers"),
      headers:{
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      },
      body: jsonEncode({"phone": phoneNumber })
    );
    return balanceModelFromJson(response.body);
  }

  Future<List<TransactionModel>> getTransactions(String address) async {
    var response = await client.post(
      Uri.parse("$baseUrl/auth/txs"),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      },
      body: jsonEncode({"address": address })
    );
    return transactionModelFromJson(response.body);
  }

  Future<ResetPinResponseModel> setPinCode(String phone, String oldPin, String newPin) async {
    var response = await client.post(
      Uri.parse("$baseUrl/auth/reset-pin"),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      },
      body: jsonEncode({"phone": phone, "old_pin": oldPin, "new_pin": newPin })
    );
    return resetPinResponseModelFromJson(response.body);
  }

  Future<TranferResponseModel> transfer(String to, String from, String amount, String pin, String symbol, String reason) async {
    var response = await client.post(
      Uri.parse("$baseUrl/auth/transfer"),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      },
      body: jsonEncode({
        "to": to ,
        "from": from ,
        "amount": amount ,
        "pin": pin ,
        "symbol": symbol ,
        "reason": reason ,
      })
    );
    return tranferResponseModelFromJson(response.body);
  }
}