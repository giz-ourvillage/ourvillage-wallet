import "package:flutter/material.dart";
import "package:get/get.dart";

void openBottomSheet({double height = 300, Widget content = const Placeholder()}){
  Get.bottomSheet( 
    Container(
      height: height,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.horizontal(),
      ),
      child:Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(onPressed: (){
                  Get.back();
                },
                icon: const Icon(
                  Icons.close,
                  size: 30,
                  color: Colors.red,
                ))
              ],
            ),
            const SizedBox(height: 5.0,),
            content,
          ],
        ),
      )
    ),
    barrierColor: Colors.grey.withOpacity(0.5),
    isDismissible: true,
    enableDrag: true,
  );
}