bool isNumeric(String s) {
 if (s == null) {
   return false;
 }
 return double.tryParse(s) != null;
}

String? isValidPhoneNumber(String phoneNumber){
  if(!isNumeric(phoneNumber)){
    return 'Must be a valid numeric';
  }
  if(phoneNumber.length != 9){
    return 'Must have 9 character';
  }
  return null;
}

String? isValidPin(String phoneNumber){
  if(!isNumeric(phoneNumber)){
    return 'Must be a valid numeric';
  }
  if(phoneNumber.length != 4){
    return 'Must have 4 character';
  }
  return null;
}

String? confirmPin(String current, String newPin){
  var validPin = isValidPin(newPin);
  if(validPin != null){
    return validPin;
  }
  if(current != newPin){
    return "Pin confirmation don't match";
  }
  return null;
}

String? validateAmountToSend(String amount, int validBalance){
  if(!isNumeric(amount)){
    return 'Please enter a valid amount';
  }
  if(double.parse(amount) <= 0) {
    return 'Your amount must be greate than zero';
  }
  if(double.parse(amount) > validBalance) {
    return 'You dont have enought fund';
  }
  return null;
}
