import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class CreateAccount extends StatelessWidget {
  const CreateAccount({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(1),
        child: Column(
          children: [
            const Text(
              "#065#",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 14.0),
            Center(
              child: Text(
                translation.descCreateAccount.tr,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ),
      );
  }
}