import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;
import 'package:ourvillage_wallet/models/community_fund_model.dart';

class CommunityFund extends StatefulWidget {
  const CommunityFund({super.key});

  @override
  State<CommunityFund> createState() => _CommunityFundState();
}

class _CommunityFundState extends State<CommunityFund> {
  final AppController appController = Get.put(AppController());
  var loading = true;
  late CommunityFundModel communityFund;

  void fetchData() async {
    var result = await this.appController.remoteService.getCommunityFund();
    setState(() {
      loading = false;
      communityFund = result;
    });
  }

  @override
  initState() {
    super.initState();
    fetchData();
  }

  int getBalanceBaseOnVillage(){
    int solde = 0;
    if(appController.activeVillage.tokenSymbole == 'MBA'){
      solde = communityFund.mba;
    }

    if(appController.activeVillage.tokenSymbole == 'MBIP'){
      solde = communityFund.mbip;
    }

    if(appController.activeVillage.tokenSymbole == 'MUN'){
      solde = communityFund.mun;
    }
    return solde;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(1),
        child: Column(
          children: ((loading) ? [ const CircularProgressIndicator(
                color: Colors.red,
              )] : [
                Center(
                    child: Text(
                      translation.descCommunityFundBalance.tr,
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const SizedBox(height: 14.0),
                  Text(
                    "${getBalanceBaseOnVillage()} ${appController.activeVillage.tokenSymbole}",
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w700,
                    ),
                  )
                ])
            ),
          );
        }
}