import 'package:flutter/material.dart';
import 'package:get/get.dart';
enum Languages { french, english }

class LanguageSelection extends StatefulWidget {
  const LanguageSelection({super.key});

  @override
  State<LanguageSelection> createState() => _LanguageSelectionState();
}

class _LanguageSelectionState extends State<LanguageSelection> {
  Languages? _languages;

  @override
  void initState() {
    var locale = Get.locale;
    if(locale?.languageCode == 'fr'){
      _languages = Languages.french;
    }

    if(locale?.languageCode == 'en'){
      _languages = Languages.english;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          ListTile(
            title: const Text('Francais'),
            leading: Radio<Languages>(
              value: Languages.french,
              groupValue: _languages,
              onChanged: (Languages? value) {
                const locale = Locale('fr', 'FR');
                Get.updateLocale(locale);
                setState(() {
                  _languages = value;
                });
              },
            ),
          ),
          ListTile(
            title: const Text('English'),
            leading: Radio<Languages>(
              value: Languages.english,
              groupValue: _languages,
              onChanged: (Languages? value) {
                const locale = Locale('en', 'US');
                Get.updateLocale(locale);
                setState(() {
                  _languages = value;
                });
              },
            ),
          ),
        ],
      );
  }
}