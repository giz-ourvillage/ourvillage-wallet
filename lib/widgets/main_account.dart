import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/screens/send_screen.dart';
import 'package:ourvillage_wallet/utils/bottom_sheet.dart';
import 'package:ourvillage_wallet/widgets/cummunity_fund.dart';
import 'package:ourvillage_wallet/widgets/receive_fund.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class MainAccount extends StatefulWidget {
  
  const MainAccount({super.key});

  @override
  State<MainAccount> createState() => _MainAccountState();
}

class _MainAccountState extends State<MainAccount> {
  final AppController appController = Get.put(AppController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.grey[200],
        ),
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 30, 15, 30),
          child: Column(
            children: [
              Text(
                appController.activeVillage.name,
                style: const TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500
                ),
              ),
              const SizedBox(height: 10,),
              Text(
                translation.descAccount.tr,
                style: const TextStyle(
                  fontSize: 12.0,
                ),
              ),
              const SizedBox(height: 10,),
              Obx(() => Text(
                  "${appController.balanceToShow} ${appController.activeVillage.tokenSymbole}",
                  style: const TextStyle(
                    fontSize: 26.0,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      const SizedBox(height: 10.0,),
      Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.grey[200],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 30, 15, 30),
          child: Column(
            children: [    
              Text(
                translation.descAccountActions.tr,
                style: const TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500
                ),
              ),
              const SizedBox(height: 20.0,),   
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  FilledButton(
                    onPressed: (){
                      Get.to(const SenDScreen());
                    },
                    style: FilledButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    child: Text(
                      translation.buttonSent.tr,
                    ),
                  ),
                  FilledButton(
                    onPressed: (){
                      openBottomSheet(
                        height: 230,
                        content: const ReceiveFund()
                      );
                    },
                    style: FilledButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    child: Text(
                      translation.buttonReceive.tr,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15.0,),
              FilledButton(
                onPressed: (){
                  openBottomSheet(
                    height: 200,
                    content: const CommunityFund()
                  );
                },
                style: FilledButton.styleFrom(
                  backgroundColor: Colors.red,
                ),
                child: Text(
                  translation.buttonCummunityFund.tr,
                ),
              )
            ],
          ),
        ),
      ),
      ]
    );
  }
}
