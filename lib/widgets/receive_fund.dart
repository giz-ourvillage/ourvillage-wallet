import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class ReceiveFund extends StatefulWidget {
  const ReceiveFund({super.key});

  @override
  State<ReceiveFund> createState() => _ReceiveFundState();
}

class _ReceiveFundState extends State<ReceiveFund> {
  final AppController appController = Get.put(AppController());

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(1),
        child: Column(
          children: [
            Center(
              child: Text(
                translation.descReceiveVouchers.tr,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            Text(
              appController.user.phoneNumber.substring(3),
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w700,
              ),
            )
          ],
        ),
      );
  }
}