import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';

import 'package:ourvillage_wallet/screens/reset_pin_screen.dart';
import 'package:ourvillage_wallet/utils/bottom_sheet.dart';
import 'package:ourvillage_wallet/widgets/language_selection.dart';
import 'package:ourvillage_wallet/widgets/user_account.dart';
import 'package:ourvillage_wallet/widgets/village_selection.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation;

class Setting extends StatefulWidget {
  const Setting({super.key});

  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  final AppController appController = Get.put(AppController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const UserAccount(),
        Card(
          child: ListTile(
            title: Text(
              translation.labelVillage.tr,
              style: const TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            subtitle: Text(
              translation.descVillage.tr,
              style: const TextStyle(
                fontSize: 12.0,
              ),
            ),
            onTap: (){
              openBottomSheet(
                height: 285,
                content: const VillageSelection()
              );
            },
          ),
        ),
        Card(
          child: ListTile(
            title: Text(
              translation.labelLanguage.tr,
              style: const TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: Text(
              Get.locale != null ? "${Get.locale?.languageCode}" : "Fr"
            ),
            subtitle: Text(
              translation.descLanguage.tr,
              style: const TextStyle(
                fontSize: 12.0,
              ),
            ),
            onTap: (){
              openBottomSheet(
                height: 200,
                content: const LanguageSelection()
              );
            },
          ),
        ),
        Card(
          child: ListTile(
            title: Text(
              translation.pinCodeHelpText.tr,
              style: const TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: const Text(
              "******"
            ),
            subtitle: Text(
              translation.descPinCode.tr,
              style: const TextStyle(
                fontSize: 12.0,
              ),
            ),
            onTap: (){
              Get.to(const ResetPinScreen());
            },
          ),
        ),
      ],
    );
  }
}