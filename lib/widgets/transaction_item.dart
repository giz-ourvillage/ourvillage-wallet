import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/models/transaction_model.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation_key;

class TransactionItem extends StatefulWidget {
  const TransactionItem({Key? key, required this.transaction}): super(key: key);

  final TransactionModel transaction;

  @override
  State<TransactionItem> createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {
  late String date;

  @override
  initState() {
    super.initState();
    date = DateTime.fromMillisecondsSinceEpoch(widget.transaction.timestamp * 1000).toString();
  }

  getLabe() {
    return widget.transaction.role == 'recipient' ? translation_key.labelRecipientTransaction.trParams({
      'number': widget.transaction.altMetadataId
    }) : translation_key.labelSentTransaction.trParams({
      'number': widget.transaction.altMetadataId
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(
          widget.transaction.role == 'recipient' ? Icons.arrow_downward_outlined : Icons.arrow_upward_outlined
        ),
        title: Text(
          getLabe(),
          style: const TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        trailing: Text(
          "${widget.transaction.tokenValue} ${widget.transaction.tokenSymbol}",
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          "${date}",
          style: const TextStyle(
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }
}