import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/widgets/transaction_item.dart';

class Transactions extends StatefulWidget {
  const Transactions({super.key});

  @override
  State<Transactions> createState() => _TransactionsState();
}

class _TransactionsState extends State<Transactions> {
  final List<String> entries = <String>['A', 'B', 'C'];
  final AppController appController = Get.put(AppController());

  Future<void> _pullRefresh() async {
    await appController.loadTransactions();
  }

  @override
  Widget build(BuildContext context) {
    return Obx( () => RefreshIndicator(
      onRefresh: _pullRefresh,
      color: Colors.red,
      child: ListView.builder(
        itemCount: appController.transactions.length,
        reverse: true,
        itemBuilder: (BuildContext context, int index) {
        return TransactionItem(transaction: appController.transactions[index]);
      }),
    ));
  }
}