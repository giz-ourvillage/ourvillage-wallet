import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';
import 'package:ourvillage_wallet/locale/translation_keys.dart' as translation_key;

class UserAccount extends StatefulWidget {
  const UserAccount({super.key});

  @override
  State<UserAccount> createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  final AppController appController = Get.put(AppController());

  void logout(){
    var snackBar = SnackBar(
      content: Text(translation_key.labelLogout.tr),
      action: SnackBarAction(
        label: 'Okay',
        onPressed: () {
          appController.clearAllAndLogout();
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: const Icon(
          Icons.person
        ),
        title: Text(
          appController.user.username,
          style: const TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        trailing: IconButton(
          onPressed: logout,
          style: IconButton.styleFrom(
            backgroundColor: Colors.red,
          ),
          icon: const Icon(Icons.logout),
        ),
        subtitle: Text(
          appController.user.phoneNumber,
          style: const TextStyle(
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }
}