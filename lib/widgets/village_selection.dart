import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ourvillage_wallet/controllers/app_controller.dart';

enum Villages { bameka, batoufam, fondjomekwet }

class VillageSelection extends StatefulWidget {
  const VillageSelection({super.key});

  @override
  State<VillageSelection> createState() => _VillageSelectionState();
}

class _VillageSelectionState extends State<VillageSelection> {
  final AppController appController = Get.put(AppController());
  Villages? _village;

  void changeVillage(Villages? value) {
    setState(() {
      _village = value;
    });
    appController.changeActiveVillage(value.toString().split('.').last);
  }

  void changeProcess(name) {
    if(name == "Batoufam"){
      _village = Villages.batoufam;
    }

    if(name == "Bameka"){
      _village = Villages.bameka;
    }

    if(name == "Fondjomekwet"){
      _village = Villages.fondjomekwet;
    }
  }

  @override
  initState() {
    super.initState();
    changeProcess(appController.activeVillage.name);
  }
  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          ListTile(
            title: const Text('Bameka'),
            leading: Radio<Villages>(
              value: Villages.bameka,
              groupValue: _village,
              onChanged: changeVillage,

            ),
          ),
          ListTile(
            title: const Text('Batoufam'),
            leading: Radio<Villages>(
              value: Villages.batoufam,
              groupValue: _village,
              onChanged: changeVillage,
            ),
          ),
          ListTile(
            title: const Text('Fondjomekwet'),
            leading: Radio<Villages>(
              value: Villages.fondjomekwet,
              groupValue: _village,
              onChanged: changeVillage,

            ),
          ),
        ],
      );
  }
}